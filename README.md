# gitlab-rulez

Compute and apply changes to the settings of the projects in a GitLab instance
based on a set of YAML rules.

## Installation

For development purposes, you can run the tool locally like this:

```shell
# preferred syntax
python -m gitlab_rulez --help
# deprecated syntax
./gitlab-rulez --help
```

For production use, install the tool as a dependency e.g. in a Python
virtual environment:

```shell
# create venv
python -m venv venv
source venv/bin/activate

# install gitlab-rulez
pip install git+https://gitlab.apertis.org/infrastructure/gitlab-rulez.git

# run it
gitlab-rulez --help
```

## Usage

First, configure the GitLab access credentials for python-gitlab:
https://python-gitlab.readthedocs.io/en/stable/cli-usage.html#configuration

Create a `rulez.yaml` file with expected GitLab setting rules, see `sample-rulez.yaml`.

Then check what currently violates the rules so far defined:

    gitlab-rulez diff ./rulez.yaml

If what gitlab-rulez highlights seem sensible, tell it to poke your GitLab
instance until it is happy:

    gitlab-rulez apply ./rulez.yaml

To limit the changes to a single project, use `--filter`:

    gitlab-rulez apply ./rulez.yaml --filter pkg/target/systemd
